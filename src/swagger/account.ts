/**
 * @swagger
 * /searchAccount:
 *  post:
 *    description: Use to get search Account
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *          properties:
 *            accountNumber:
 *              type: string
 *            phoneNumber:
 *              type: string
 *            ssn:
 *              type: string
 *          required:
 *              - accountNumber
 *          example:
 *            {"accountNumber":"123123"}
 *    responses:
 *       "200":
 *          description: A successful response
 *
 */
