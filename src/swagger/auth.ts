/**
 * @swagger
 * /login:
 *  post:
 *    description: Login to get Token
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *          properties:
 *            userName:
 *              type: string
 *            password:
 *              type:string
 *          example:
 *              {
 *               "userName":"string",
 *               "password":"string"
 *              }
 *    responses:
 *       "200":
 *          description: A successful response
 *
 */
