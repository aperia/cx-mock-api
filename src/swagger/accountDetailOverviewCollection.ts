/**
 * @swagger
 * /accountDetailOverviewCollection:
 *  post:
 *    description: Use to get account detail for Overview Collection
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *          properties:
 *            accountNumber:
 *              type: string
 *          required:
 *              - accountNumber
 *          example:
 *            {"accountNumber": "17239211111"}
 *    responses:
 *       "200":
 *          description: A successful response
 *
 */
