import swaggerJsdoc from 'swagger-jsdoc';
let urlServer = 'http://localhost:10000/api/cxc';
let apiUrl = 'src/swagger/*.ts';
if (process.env.NODE_ENV === 'production') {
  urlServer = 'http://cx-mockapi-int.local/api/cxc';
  apiUrl = './swagger/*.js';
}

const options = {
  definition: {
    openapi: '3.0.0',
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    },
    info: {
      title: 'Mock API -- CX Collections Express API with Swagger',
      version: '0.1.0',
      description:
        'This is a simple CRUD API application made with Express and documented with Swagger',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html'
      },
      contact: {
        name: 'Aperia - Collection Team'
      }
    },
    servers: [
      {
        url: urlServer
      }
    ]
  },
  apis: [apiUrl]
};
export const specs = swaggerJsdoc(options);
