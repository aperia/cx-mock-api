/**
 * @swagger
 * /getAccountDetails:
 *  post:
 *    description: Use to get account detail
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *          properties:
 *            bodyRequest:
 *              type: object
 *          required:
 *              - bodyRequest
 *          example:
 *            {"bodyRequest": {}}
 *    responses:
 *       "200":
 *          description: A successful response
 *
 */
