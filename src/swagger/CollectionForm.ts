/**
 * @swagger
 * /collectionForm/checkCallResultStatus:
 *  post:
 *    description: Use to get search Account
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *          properties:
 *            accountId:
 *              type: string
 *            callResultType:
 *              type: number
 *        example:
 *            {"accountId":"123123","callResultType":1}
 *    responses:
 *       "200":
 *          description: A successful response
 *
 */
