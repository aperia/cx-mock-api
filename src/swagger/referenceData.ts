/**
 * @swagger
 * /referenceData/getReferenceDataOptions:
 *  post:
 *    description: Use to get reference Data Options
 *    security:
 *      - bearerAuth: []
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *          properties:
 *            accountId:
 *              type: string
 *            componentID:
 *              type: string
 *        example:
 *            {"accountId":"123123","componentID":"Group_Memo"}
 *    responses:
 *       "200":
 *          description: A successful response
 *
 */
