import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import cors from 'cors';
import swaggerUI from 'swagger-ui-express';
import { specs } from './swagger/swaggerDocument';

// routes
import {
  accountRouter,
  collectionFormRouter,
  memoRouter,
  referenceDataRouter,
  transactionRouter,
  paymentRouter
} from './routers';

//middleware
import { ErrorHandler } from './middleware';
const virtualDirPath = process.env.virtualDirPath || '';

dotenv.config();

// initial express app
const app = express();

// cors
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// parse cookie
app.use(cookieParser(process.env.COOKIE_SECRET_KEY));

// home page
app.get(virtualDirPath, (req, res) => {
  res.send('Welcome to Collections API');
});

// apis
const baseApiUrl = virtualDirPath + '/api/v1/mockApi';
app.use(baseApiUrl + '/account', accountRouter);
app.use(baseApiUrl + '/collectionForm', collectionFormRouter);
app.use(baseApiUrl + '/memo', memoRouter);
app.use(baseApiUrl + '/referenceData', referenceDataRouter);
app.use(baseApiUrl + '/transaction', transactionRouter);
app.use(baseApiUrl + '/payment', paymentRouter);

app.use(
  virtualDirPath + '/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(specs, { explorer: true })
);
// request not found
app.use((req, res) => {
  res.status(404).json({ message: 'Request not found' });
});

// error handler
app.use(ErrorHandler);
// start server
const port = process.env.PORT || 10000;
app.listen(port, () => console.log('Collections mock-api is running...'));
