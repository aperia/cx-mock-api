import { paymentController } from '../controllers';
import { AccountIdRequired } from '../middleware';

import { Router } from 'express';

// init router
const paymentRouter = Router({ strict: true, caseSensitive: true });

paymentRouter.post(
  '/getPayments',
  AccountIdRequired,
  paymentController.getPayments
);

paymentRouter.post(
  '/getMoreDetailPayment',
  AccountIdRequired,
  paymentController.getMoreDetailPayment
);
paymentRouter.post(
  '/cancelPendingPayments',
  AccountIdRequired,
  paymentController.cancelPendingPayments
);

paymentRouter.post('/getBankAccounts', paymentController.getBankAccounts);

paymentRouter.post(
  '/validateBankAccountInfo',
  paymentController.validateBankAccountInfo
);

paymentRouter.post(
  '/registerBankAccount',
  AccountIdRequired,
  paymentController.registerBankAccount
);

paymentRouter.post(
  '/getDetailPaymentInfo',
  AccountIdRequired,
  paymentController.getDetailPaymentInfo
);

paymentRouter.post(
  '/getDetailAccountPaymentInfo',
  AccountIdRequired,
  paymentController.getDetailAccountPaymentInfo
);

export { paymentRouter };
