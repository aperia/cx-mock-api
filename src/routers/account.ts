import { Router } from 'express';
import { accountController } from '../controllers';

export const accountRouter = Router({ strict: true, caseSensitive: true });

accountRouter.post('/searchAccount', accountController.searchAccount);

accountRouter.post('/getAccountDetails', accountController.getAccountDetails);

accountRouter.post('/getOverviewAccount', accountController.getOverviewAccount);

accountRouter.post(
  '/getOverviewCollection',
  accountController.getOverviewCollection
);

accountRouter.post('/getMoreAccountInfo', accountController.getMoreAccountInfo);

// ------------------------ CXC-59 update external status ----------------------------

accountRouter.post(
  '/externalStatusRefData',
  accountController.externalStatusRefData
);

accountRouter.post(
  '/externalStatusReasonRefData',
  accountController.externalStatusReasonRefData
);

accountRouter.post(
  '/updateAccountExternalStatusCode',
  accountController.updateAccountExternalStatusCode
);

// ------------------------ CXC-59 update external status ----------------------------
