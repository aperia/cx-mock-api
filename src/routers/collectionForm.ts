import { Router } from 'express';
import { collectionFormController } from '../controllers';

const collectionFormRouter = Router({ strict: true, caseSensitive: true });

// Reference Data
collectionFormRouter.post(
  '/getContactReferData',
  collectionFormController.getContactReferData
);

collectionFormRouter.post(
  '/getCallResultReferData',
  collectionFormController.getCallResultReferData
);

// Promise To Pay
collectionFormRouter.post(
  '/checkCallResultStatus',
  collectionFormController.checkCallResultStatus
);

collectionFormRouter.post(
  '/updateCallResultStatus',
  collectionFormController.updateCallResultStatus
);

export { collectionFormRouter };
