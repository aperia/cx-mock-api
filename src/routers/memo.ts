import { memoController } from '../controllers';

import { Router } from 'express';

// init router
const memoRouter = Router({ strict: true, caseSensitive: true });

memoRouter.post('/checkMemoType', memoController.checkMemoType);

memoRouter.post('/addChronicleMemo', memoController.addChronicleMemo);
memoRouter.post('/deleteChronicleMemo', memoController.removeChronicleMemo);
memoRouter.post('/getChronicleMemosList', memoController.getListChronicleMemo);
memoRouter.post('/updateChronicleMemo', memoController.updateChronicleMemo);

// CisMemo
memoRouter.post('/getCisMemos', memoController.getCisMemos);
memoRouter.post('/addCisMemo', memoController.addCisMemo);
memoRouter.post('/updateCISMemo', memoController.updateCISMemo);
memoRouter.post('/deleteCISMemo', memoController.deleteCISMemo);

export { memoRouter };
