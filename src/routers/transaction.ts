import { Router } from 'express';
import { transactionController } from '../controllers';

export const transactionRouter = Router({ strict: true, caseSensitive: true });

transactionRouter.post(
  '/getPendingAuthTransactions',
  transactionController.getPendingAuthTransactions
);

transactionRouter.post(
  '/getStatementMonthList',
  transactionController.statementMonthList
);

transactionRouter.post(
  '/getMontlyStatementSummary',
  transactionController.getStatementOverview
);

transactionRouter.post(
  '/getTransactionList',
  transactionController.getTransactionList
);

transactionRouter.post(
  '/getTransactionDetail',
  transactionController.getTransactionDetail
);

transactionRouter.post(
  '/getAccountStatementHistory',
  transactionController.getAccountStatementHistory
);

transactionRouter.post(
  '/getPendingAuthTransactionDetail',
  transactionController.getPendingAuthTransactionDetail
);
