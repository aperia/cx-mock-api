import { referenceDataController } from '../controllers/referenceData';

import { Router } from 'express';

// init router
const referenceDataRouter = Router({ strict: true, caseSensitive: true });

referenceDataRouter.post(
  '/getStateListRefData',
  referenceDataController.getStateListRefData
);

referenceDataRouter.post(
  '/getReferenceDataOptions',
  referenceDataController.getReferenceDataOptions
);

export { referenceDataRouter };
