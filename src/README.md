# Examples

```javascript
/** SIMPLE EXAMPLE **/

// input
{
  "response": {
    "simpleNumber": "<%mock.random.number%>",
    "simple5Words": "<%mock.random.words(5)%>",
    "nested": {
      "testCombine": "<%mock.random.city%> some thing string <%mock.random.cityPrefix%>"
    }
  }
}

// output
{
  response: {
    simpleNumber: 8736473242,
    simple5Words: "granular IB deposit Luisamouth",
    nested: {
      testCombine: "Luisamouth some thing string West"
    }
  }
}
```

```javascript
/** SUPPORTED CONSTRAINT FIELD **/

// input
{
  "card": {
    "code": "<%mock.random.number(1)%>",
    "codeType": "<%mock.custom.get('card.code') === 1 ? 'Success' : 'Failure'%>"
  }
}

// output
{
  card: {
    code: 1,
    codeType: "Success"
  }
}
```

```javascript
/** SUPPORTED GENERATE LOOP, EMPTYABLE **/

// loop
"_config": {
  "response.data.data": {
    "count": 3
  }
}

// emptyAble
{
  "card": {
    "code": "<%mock.custom.emptyAble(mock.random.number())%>"
  }
}

```

# DataType list

## root variable

mock - prefix for all data types

## random

random.number 33467 <br />
random.float 71069.02 <br />
random.arrayElement b <br />
random.arrayElements [ 'a', 'b', 'c' ] <br />
random.objectElement car <br />
random.uuid 62373f84-047f-4e47-910d-3ffe1fc7181b <br />
random.boolean true <br />
random.word deposit <br />
random.words granular IB <br />
random.image http://placeimg.com/640/480/animals <br />
random.locale en <br />
random.alpha d <br />
random.alphaNumeric u <br />
random.hexaDecimal 0xA <br />

## address

address.zipCode 34741-8349 <br />
address.zipCodeByState 71608-3739 <br />
address.city Luisamouth <br />
address.cityPrefix West <br />
address.citySuffix haven <br />
address.streetName Whitney Haven <br />
address.streetAddress 367 Russel Land <br />
address.streetSuffix Glen <br />
address.streetPrefix a <br />
address.secondaryAddress Apt. 275 <br />
address.county Bedfordshire <br />
address.country Equatorial Guinea <br />
address.countryCode IT <br />
address.state Oklahoma <br />
address.stateAbbr NM <br />
address.latitude 22.6021 <br />
address.longitude -85.1758 <br />
address.direction East <br />
address.cardinalDirection South <br />
address.ordinalDirection Northwest <br />
address.nearbyGPSCoordinate [ '-51.0238', '42.9450' ] <br />
address.timeZone America/Halifax <br />

## commerce

commerce.color gold <br />
commerce.department Sports <br />
commerce.productName Practical Steel Computer <br />
commerce.price 725.00 <br />
commerce.productAdjective Generic <br />
commerce.productMaterial Rubber <br />
commerce.product Sausages <br />
commerce.productDescription The Football Is Good For Training And Recreational Purposes <br />

## company

company.suffixes [ 'Inc', 'and Sons', 'LLC', 'Group' ] <br />
company.companyName Frami and Sons <br />
company.companySuffix Group <br />
company.catchPhrase Self-enabling secondary methodology <br />
company.bs redefine killer mindshare <br />
company.catchPhraseAdjective Digitized <br />
company.catchPhraseDescriptor human-resource <br />
company.catchPhraseNoun utilisation <br />
company.bsAdjective transparent <br />
company.bsBuzz iterate <br />
company.bsNoun ROI <br />

## date

date.past 2020-04-03T10:18:29.914Z <br />
date.future 2021-02-20T08:40:42.540Z <br />
date.between Invalid Date <br />
date.recent 2020-10-26T22:31:28.893Z <br />
date.soon 2020-10-28T01:18:35.442Z <br />
date.month September <br />
date.weekday Sunday <br />

## finance

finance.account 26145098 <br />
finance.accountName Money Market Account <br />
finance.routingNumber 366471933 <br />
finance.mask 4117 <br />
finance.amount 328.69 <br />
finance.transactionType withdrawal <br />
finance.currencyCode GIP <br />
finance.currencyName UIC-Franc <br />
finance.currencySymbol ₩ <br />
finance.bitcoinAddress 1TxW1WjsLJKEGLe4BkcoE66UP <br />
finance.litecoinAddress 3eusqUgrQbeLFj3gQVPJH1rGZ1P9uA4T <br />
finance.creditCardNumber 6011-8981-9341-3654 <br />
finance.creditCardCVV 708 <br />
finance.ethereumAddress 0xbaabe8f01bb07db14b260c67071e98deb1d6eec6 <br />
finance.iban BR6406637113673034037697628Q4 <br />
finance.bic IDAESHU1 <br />
finance.transactionDescription withdrawal transaction at Barton Inc using card ending with **_4832 for ERN 364.76 in account _**55104071 <br />

## name

name.firstName Nona <br />
name.lastName Huels <br />
name.findName Melvin Osinski <br />
name.jobTitle Senior Implementation Planner <br />
name.gender Neither <br />
name.prefix Ms. <br />
name.suffix IV <br />
name.title Dynamic Applications Agent <br />
name.jobDescriptor District <br />
name.jobArea Functionality <br />
name.jobType Producer <br />

## phone

phone.phoneNumber 1-638-717-2782 x39058 <br />
phone.phoneNumberFormat 652-426-3064 <br />
phone.phoneFormats !##.!##.#### x#### <br />

## customize

custom.get - Get value from other property <br />
custom.getOne <br />
custom.emptyAble <br />
custom.randomFloat(2000) 1798.59 <br />
custom.randomFloat(2000,4) 198.5926 <br />
