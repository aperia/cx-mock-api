import { Request, Response, Handler } from 'express';
import faker from 'faker';
import {
  pick,
  get,
  set,
  isNumber,
  isEmpty,
  findIndex,
  orderBy,
  filter
} from 'lodash';
import {
  gen400,
  getTimeValueOfStartDate,
  mockGeneratorPromise
} from '../helpers';
import {
  PAGE_SIZE_MEMO_CHRONICLE,
  MAXIMUM_MEMO_CHRONICLE,
  MEMO_TYPE,
  MOCK_ACCOUNT_ID,
  MOCK_CIS_MEMO,
  MOCK_CHRONICLE_MEMO
} from '../configs';

import referenceDataOptions from './referenceData/referenceDataOptions.json';

interface MemoResponse {
  memos: any[];
}

interface RefData {
  value: string;
  description: string;
}
interface CISMemoItem {
  memoType: RefData;
  [other: string]: any;
}

const { CIS_Source } = referenceDataOptions;

const chronicleMemo = {};
const cisMemos = {};

export const memoController: Record<string, Handler> = {
  checkMemoType: (req, res) => {
    const { accountId } = req.body;

    if (!accountId) {
      return gen400(res, { message: 'AccountId is required' });
    }

    let type = [MEMO_TYPE.CHRONICLE, MEMO_TYPE.CIS][faker.random.number(1)];

    if (MOCK_CIS_MEMO.includes(accountId)) {
      type = MEMO_TYPE.CIS;
    } else if (MOCK_CHRONICLE_MEMO.includes(accountId)) {
      type = MEMO_TYPE.CHRONICLE;
    }
    res.status(200).json({ type });
  },

  removeChronicleMemo: (req, res) => {
    const { accountId, memoIdentifier } = req.body;
    if (!accountId) {
      return gen400(res, { message: 'AccountId is required' });
    }
    if (!memoIdentifier) {
      return gen400(res, { message: 'memoIdentifier is required' });
    }
    const { data = [] } = get(chronicleMemo, [accountId], {});

    if (data.length >= MAXIMUM_MEMO_CHRONICLE) {
      return gen400(res, { message: 'Memo is over limitation' });
    }

    const indexMemo = data.findIndex(
      (memo: any) => memo['memoIdentifier'] === memoIdentifier
    );

    if (indexMemo === -1) {
      return gen400(res, { message: 'Memo is not found' });
    }
    data.splice(indexMemo, 1);
    set(chronicleMemo, [accountId], {
      data: buildChronicleMemo(data),
      totalGenerated: data?.length - 1
    });
    res.status(200).json({ status: 1 });
  },

  addChronicleMemo: (req: Request, res: Response) => {
    const {
      accountId,
      memoText,
      memoType,
      overrideRetentionDate,
      memoDate = new Date(),
      memoTime = new Date()
    } = req.body;
    if (!accountId) {
      return gen400(res, { message: 'AccountId is required' });
    }

    if (!memoType) {
      return gen400(res, { message: 'Type is required' });
    }

    if (memoText.toLowerCase() === 'fail') {
      return gen400(res, { message: 'Add memo failed' });
    }

    const { data = [] } = get(chronicleMemo, [accountId], {});
    if (data.length >= MAXIMUM_MEMO_CHRONICLE) {
      return gen400(res, { message: 'Memo is over limitation' });
    }

    const newMemo = [
      {
        memoIdentifier: data?.length + 2,
        memoOperatorIdentifier: faker.random.alphaNumeric(3).toUpperCase(),
        memoType,
        memoText,
        memoDate,
        memoTime,
        overrideRetentionDate
      }
    ];

    set(chronicleMemo, [accountId], {
      data: buildChronicleMemo(newMemo.concat(data))
    });
    res.status(200).json({ status: 1 });
  },

  updateChronicleMemo: (req, res) => {
    const {
      accountId,
      memoIdentifier,
      memoText,
      memoType,
      overrideRetentionDate
    } = req.body;
    if (!accountId || !memoIdentifier || !memoText || !memoType) {
      return gen400(res, {
        message: 'AccountId / memoIdentifier / memoText / memoType is required'
      });
    }
    // Demo Update Case Fail
    if (/fail/i.test(memoText)) {
      return res.status(400).json({
        message: 'updated Failed'
      });
    }

    const allExistedMemos = get(chronicleMemo, [accountId, 'data'], []);

    const updateMemoIndex = findIndex(allExistedMemos, {
      memoIdentifier: parseInt(memoIdentifier)
    });

    const existedMemo = allExistedMemos[updateMemoIndex];

    const updateMemo = {
      ...existedMemo,
      memoType,
      memoText,
      overrideRetentionDate
    };
    set(chronicleMemo, [accountId, 'data', updateMemoIndex], updateMemo);
    res.status(200).json({
      message: 'updated',
      memo: updateMemo
    });
  },

  getListChronicleMemo: async (req: Request, res: Response) => {
    try {
      // collect data
      let {
        startSequence = 0,
        endSequence = PAGE_SIZE_MEMO_CHRONICLE
      } = req.body;
      const { memoDate } = req.body;
      const accountId = req.body.accountId;
      startSequence = +startSequence;
      endSequence = +endSequence;

      // validation
      if (!accountId) {
        return gen400(res, { message: 'AccountId is required' });
      }

      if (endSequence < startSequence) {
        return gen400(res, {
          message: 'EndSequence must greater than StartSequence'
        });
      }

      if (accountId === MOCK_ACCOUNT_ID.MEMO_CHRONICLE_NO_MEMO_FOUND) {
        return res.status(200).json({ memos: [] });
      }

      let existedMemos = get(chronicleMemo, [accountId, 'data'], []);

      if (existedMemos.length && memoDate) {
        existedMemos = filter(existedMemos, (item: Record<string, string>) => {
          return (
            new Date(item?.memoDate).toLocaleDateString() ===
            new Date(memoDate).toLocaleDateString()
          );
        });
      }

      if (existedMemos.length !== 0 || memoDate) {
        const memosPath = existedMemos.slice(startSequence - 1, endSequence);
        return res.status(200).json({
          memos: memosPath
        });
      }

      // build generate
      const data: any = await mockGeneratorPromise(
        getGenerateFileById(accountId)
      );
      let newGenerateMemos = data.memos;
      newGenerateMemos = newGenerateMemos.sort(
        (a: any, b: any) =>
          new Date(b.memoDate).valueOf() - new Date(a.memoDate).valueOf()
      );
      newGenerateMemos = buildChronicleMemo(newGenerateMemos);

      set(chronicleMemo, [accountId], {
        data: newGenerateMemos
      });
      res.status(200).json({
        memos: newGenerateMemos.slice(startSequence - 1, endSequence)
      });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  },

  // CIS MEMO
  getCisMemos: async (req, res) => {
    // collect data
    const { accountId } = req.body;
    let {
      startSequence = 0,
      endSequence = PAGE_SIZE_MEMO_CHRONICLE,
      startDate,
      endDate
    } = req.body;
    startSequence = parseInt(startSequence);
    endSequence = parseInt(endSequence);
    startDate = startDate && getTimeValueOfStartDate(startDate);
    endDate = startDate && getTimeValueOfStartDate(endDate, 1);

    // validation
    if (!accountId || !isNumber(startSequence) || !isNumber(endSequence)) {
      return gen400(res, {
        message:
          'AccountId is required or startSequence endSequence incorrect format'
      });
    }

    if (endSequence < startSequence) {
      return gen400(res, {
        message: 'EndSequence must greater than StartSequence'
      });
    }

    if (accountId === MOCK_ACCOUNT_ID.MEMO_CIS_NO_MEMO_FOUND) {
      return res.send({ memos: [] });
    }

    const sendRequiredMemos = (allMemos: Record<string, any>[]) => {
      // filter by Date
      let filteredMemo = allMemos;
      if (startDate && endDate) {
        filteredMemo = allMemos.filter(memo => {
          const memoDate = new Date(memo.memoDate).valueOf();
          return memoDate >= startDate && memoDate <= endDate;
        });
      }
      res.status(200).json({
        memos: filteredMemo.slice(startSequence - 1, endSequence),
        startSequence,
        endSequence
      });
    };

    // get in cache data
    const allExistedMemos = get(cisMemos, [accountId, 'data'], []);
    if (!isEmpty(allExistedMemos)) {
      return sendRequiredMemos(allExistedMemos);
    }

    // build generate
    const jsonFile =
      accountId === MOCK_ACCOUNT_ID.MEMO_CIS_999_MEMO
        ? 'memo/memoCIS999.json'
        : 'memo/memoCIS.json';
    try {
      const data = await mockGeneratorPromise<MemoResponse>(jsonFile);
      // Put Sequence Number in generated Data.
      const orderByDateMemos = orderBy(
        data.memos,
        [item => new Date(item.memoDate)],
        'asc'
      );
      const addSequenceNumberMemos = orderByDateMemos.map(
        (item: any, index: number) => ({
          ...item,
          memoSequenceNumber: index + 1,
          memoIdentifier: index + 1
        })
      );

      const lastSequenceNumber = addSequenceNumberMemos.length;

      // store data
      set(cisMemos, [accountId, 'data'], orderCisMemo(addSequenceNumberMemos));
      set(cisMemos, [accountId, 'lastSequenceNumber'], lastSequenceNumber);
      sendRequiredMemos(orderCisMemo(addSequenceNumberMemos));
    } catch (error) {
      const { message, stack } = pick(error, 'message', 'stack');
      return res.status(500).send({ message, stack });
    }
  },

  addCisMemo: (req, res) => {
    // collect data
    const { accountId, memoText, memoType } = req.body;

    // validation
    if (!accountId || !memoText || !memoType) {
      return gen400(res, {
        message: 'AccountId / memoText / memoType is required'
      });
    }

    // DEMO CASE API FAILED
    if (memoText === 'FAIL')
      return res.status(400).json({
        message: 'failed to update'
      });

    // get in cache data
    const lastSequenceNumber = get(cisMemos, [accountId, 'lastSequenceNumber']);

    const allExistedMemos = get(
      cisMemos,
      [accountId, 'data'],
      []
    ) as CISMemoItem[];

    if (allExistedMemos && allExistedMemos.length >= 999) {
      return res.status(400).json({
        message: 'failed to add over 999 memos'
      });
    }

    const newMemo = {
      memoIdentifier: lastSequenceNumber + 1,
      memoOperatorIdentifier: faker.random.number(1000),
      memoType: {
        value: memoType,
        description: memoType
      },
      memoText,
      memoSequenceNumber: lastSequenceNumber + 1,
      memoDate: new Date(),
      memoSource: CIS_Source[faker.random.number(8)]
    } as CISMemoItem;

    // update new
    set(cisMemos, [accountId], {
      data: orderCisMemo(allExistedMemos.concat(newMemo)),
      lastSequenceNumber: lastSequenceNumber + 1
    });
    res.status(200).json({
      message: 'add memo successfully'
    });
  },

  updateCISMemo: (req, res) => {
    const { accountId, memoIdentifier, memoText, memoType } = req.body;
    if (!accountId || !memoIdentifier || !memoText || !memoType) {
      return gen400(res, {
        message: 'AccountId / memoIdentifier / memoText / memoType is required'
      });
    }
    // Demo Update Case Fail
    if (/fail/i.test(memoText)) {
      return res.status(400).json({
        message: 'updated Failed'
      });
    }

    const allExistedMemos = get(cisMemos, [accountId, 'data'], []);

    const updateMemoIndex = findIndex(allExistedMemos, {
      memoIdentifier: parseInt(memoIdentifier)
    });

    const existedMemo = allExistedMemos[updateMemoIndex];

    const updateMemo = {
      ...existedMemo,
      memoType,
      memoText,
      memoDate: new Date()
    };
    set(cisMemos, [accountId, 'data', updateMemoIndex], updateMemo);
    res.status(200).json({
      message: 'updated',
      memo: updateMemo
    });
  },

  deleteCISMemo: (req, res) => {
    const { accountId, memoIdentifier } = req.body;
    if (!accountId || !memoIdentifier) {
      return gen400(res, {
        message: 'AccountId / memoIdentifier is required'
      });
    }
    const allExistedMemos = get(cisMemos, [accountId, 'data'], []);

    const deleteMemoIndex = findIndex(allExistedMemos, {
      memoIdentifier: parseInt(memoIdentifier)
    });

    if (deleteMemoIndex >= 0) {
      allExistedMemos.splice(deleteMemoIndex, 1);
      set(cisMemos, [accountId, 'data'], allExistedMemos);
      return res.status(200).json({
        message: 'delete successfully'
      });
    }

    return res.status(400).json({
      message: 'delete failed'
    });
  }
};

const buildChronicleMemo = (memos: any) => {
  return memos.map((memo: any, index: number) => ({
    ...memo,
    memoOperatorIdentifier: memo.memoOperatorIdentifier
      .toString()
      .toUpperCase(),
    memoIdentifier: index + 1
  }));
};

const orderCisMemo = (Memos: CISMemoItem[]) =>
  orderBy(
    Memos,
    [item => item.memoType.value, 'memoSequenceNumber'],
    ['asc', 'desc']
  );

const getGenerateFileById = (accountId: string) => {
  switch (accountId) {
    case MOCK_ACCOUNT_ID.MEMO_CHRONICLE_1:
      return 'memo/memoChronicle1.json';
    default:
      return 'memo/memoChronicle.json';
  }
};
