import { Handler } from 'express';
import { pick, get, set } from 'lodash';
import {
  gen400,
  generateMessage,
  checkRequireFields,
  gen453Code,
  mockGenerator,
  mockGeneratorPromise
} from '../helpers';
import { MOCK_ARRAY_ACCOUNT_ID, MOCK_ACCOUNT_ID } from '../configs';

const accountData = {};
const searchAccounts = {};
const OTHER_ACCOUNT_ID = 'other_account_id';

export const accountController: Record<string, Handler> = {
  searchAccount: async (req, res) => {
    const { accountNumber, startSequence, endSequence } = req.body;

    if (accountNumber === MOCK_ACCOUNT_ID.NO_SEARCH_RESULT_LIST) {
      return res.send({ customers: [] });
    }

    try {
      // accountId belong to predefined accountId list
      if (MOCK_ARRAY_ACCOUNT_ID.includes(accountNumber)) {
        const data = (await mockGeneratorPromise(
          'account/searchAccount.json'
        )) as Object;
        set(data, 'customers[0].accountNumber.eValue', accountNumber);
        return res.status(200).json(data);
      }

      let otherData: any[] = get(searchAccounts, [OTHER_ACCOUNT_ID]);
      if (!otherData) {
        const { customers } = await mockGeneratorPromise(
          'account/searchAccounts.json'
        );
        otherData = customers;
        set(searchAccounts, [OTHER_ACCOUNT_ID], customers);
      }
      return res.status(200).json({
        customers: otherData.slice(
          parseInt(startSequence) - 1,
          parseInt(endSequence)
        )
      });
    } catch (error) {
      return res.status(500).json(error);
    }
  },

  getAccountDetails: (req, res) => {
    const accountId = get(req.body, ['common', 'accountId']) as string;
    mockGenerator('account/accountDetailSnapshot.json', (error, data) => {
      if (error) {
        const { message, stack } = pick(error, 'message', 'stack');
        return res.status(500).send({ message, stack });
      }

      const storeData = get(accountData, [accountId]);

      if (storeData) {
        return res.send(storeData);
      }
      set(accountData, [accountId], data);

      res.send(data);
    });
  },

  getOverviewAccount: (req, res) => {
    const { accountNumber } = req.body;
    if (!accountNumber) {
      return res.status(404).send({ message: 'accountId is required' });
    }
    mockGenerator(
      'account/accountDetailOverviewAccount.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        res.send(data);
      }
    );
  },
  getOverviewCollection: (req, res) => {
    const { accountNumber } = req.body;
    if (!accountNumber) {
      return res.status(404).send({ message: 'accountId is required' });
    }
    mockGenerator(
      'account/accountDetailOverviewCollection.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        res.send(data);
      }
    );
  },
  getMoreAccountInfo: (req, res) => {
    mockGenerator('account/accountMoreInfo.json', (error, data) => {
      if (error) {
        const { message, stack } = pick(error, 'message', 'stack');
        return res.status(500).send({ message, stack });
      }
      res.send(data);
    });
  },
  // external satus
  externalStatusRefData: (req, res) => {
    const { accountId } = req.body;
    if (!accountId) {
      return gen400(res, { message: 'AccountId is required' });
    }

    mockGenerator(
      'account/accountExternalStatusCodeRefData.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        res.send(data);
      }
    );
  },
  externalStatusReasonRefData: (req, res) => {
    const { accountId } = req.body;
    if (!accountId) {
      return gen400(res, { message: 'AccountId is required' });
    }

    mockGenerator(
      'account/accountExternalStatusReasonRefData.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        res.send(data);
      }
    );
  },
  updateAccountExternalStatusCode: (req, res) => {
    const accountId = get(req.body, ['accountId']) as string;
    const externalStatusCode = get(req.body, ['externalStatusCode']);
    const reasonCode = get(req.body, ['reasonCode']);
    const requestData = { accountId, externalStatusCode, reasonCode };

    if (!checkRequireFields(requestData)) {
      return gen400(res, {
        message: `${generateMessage(requestData)} is/are required`
      });
    }

    // handle Bussiness Error
    const mockErrorStatusCode = ['E', 'U'];
    const mockErrorReasonCode = ['97', '99'];

    if (
      mockErrorStatusCode.includes(externalStatusCode) ||
      mockErrorReasonCode.includes(reasonCode)
    ) {
      return gen453Code(res, 'This is business error message');
    }

    const existingAccountData = get(accountData, [accountId]);

    if (!existingAccountData) {
      return gen400(res, { message: 'failed status code success' });
    }

    set(
      accountData,
      [accountId, 'customers', '0', 'externalStatusCode'],
      externalStatusCode
    );
    set(accountData, [accountId, 'customers', '0', 'statusDate'], new Date());
    res.send({ status: 'update status code success' });
  }
};
