import { RequestHandler } from 'express';
import faker from 'faker';
import {
  get,
  set,
  orderBy,
  filter,
  some,
  isArray,
  cloneDeep,
  isEmpty
} from 'lodash';
import {
  mockGeneratorPromise,
  isDateBetween,
  gen453Code,
  maskData,
  hashValue,
  getLastNumOfChar
} from '../helpers';
import { MOCK_ACCOUNT_ID, BANK_ACCOUNTS, BANK_ACCOUNTS_TYPE } from '../configs';

import mockBankAccounts from './mock-json/payment/accounts.json';

const paymentData: Record<string, PaymentItem[]> = {};
const bankAccountData: Record<string, typeof mockBankAccounts> = {};

interface PaymentResponse {
  payments: PaymentItem[];
}

interface ReferData {
  value?: string;
  description: string;
  code?: string;
}

interface PaymentItem {
  paymentId: string;
  amount: number;
  schedulePaymentDate: string;
  paymentAccountNumber: {
    maskedValue: string;
    eValue: string;
  };
  confirmationNumber: string;
  paymentStatus: ReferData;
  paymentType: ReferData;
  moreInfo: {
    source: string;
    initiatedDate: string;
    bankRoutingNumber: string;
    bankName: string;
    bankCity: string;
    bankState: string;
    fee: number;
  };
}

const canceledStatus = { value: 'Canceled', description: 'Canceled' };

export const paymentController: Record<string, RequestHandler> = {
  getPayments: async (req, res, next) => {
    const {
      accountId,
      last4AccountNumber = '',
      bankRoutingNumber = '',
      confirmationNumber = '',
      paymentAmount,
      endDate,
      startDate
    } = req.body;

    // NO Data Found
    const sendPayment = (payments: any[] = []) => {
      return res.status(200).json({ payments });
    };

    if (accountId === MOCK_ACCOUNT_ID.PAYMENT_NO_DATA_FOUND) {
      sendPayment();
    }
    try {
      const payments = await getPaymentDataByAccountId(accountId);
      const searchData = filter(payments, item => {
        let isValid = false;
        isValid =
          item.paymentAccountNumber.maskedValue.includes(last4AccountNumber) &&
          item.moreInfo.bankRoutingNumber
            .toString()
            .includes(bankRoutingNumber) &&
          item.confirmationNumber.toString().includes(confirmationNumber) &&
          isDateBetween(item.schedulePaymentDate, startDate, endDate);

        if (paymentAmount) isValid = item.amount === parseFloat(paymentAmount);
        return isValid;
      });
      return sendPayment(searchData);
    } catch (error) {
      next(error);
    }
  },
  getMoreDetailPayment: async (req, res, next) => {
    const { paymentId, accountId } = req.body;
    try {
      const payments = await getPaymentDataByAccountId(accountId);
      const existMoreInfo = payments.find(item => item.paymentId === paymentId);
      if (existMoreInfo)
        return res.send({ paymentDetail: existMoreInfo.moreInfo });
      return res.send({ paymentDetail: {} });
    } catch (error) {
      next(error);
    }
  },

  cancelPendingPayments: async (req, res, next) => {
    const { accountId, paymentIds } = req.body;
    try {
      const payments = await getPaymentDataByAccountId(accountId);
      if (!isArray(paymentIds))
        return next(new Error('PaymentIds should be an array'));
      const updatedPayments = payments.map(item => {
        if (paymentIds.includes(item.paymentId)) {
          item.paymentStatus = canceledStatus;
        }
        return item;
      });
      updatePaymentDataByAccountId(accountId, updatedPayments);
      res.status(200).json({ message: 'canceled successfully' });
    } catch (error) {
      next(error);
    }
  },
  getBankAccounts: async (req, res, next) => {
    const { accountId } = req.body;
    try {
      //
      const maskedBankAccounts = maskBankAccountNumbers(
        getBankAccountByAccountId(accountId)
      );
      // check pending status in payment list

      const payments = await getPaymentDataByAccountId(accountId);

      const updatedBankAccounts = maskedBankAccounts.map(bank => {
        const eValue = get(bank, ['accountNumber', 'eValue']);

        const isPendingStatus = some(
          payments.filter(
            payment => payment.paymentAccountNumber.eValue === eValue
          ),
          {
            paymentStatus: {
              value: 'Pending',
              description: 'Pending'
            }
          }
        );
        set(bank, ['isPendingStatus'], isPendingStatus);

        return bank;
      });

      // mask Bank Routing Number and return to Client
      const maskedAccounts = updatedBankAccounts.map(bank => {
        const bankRoutingNumber = bank?.moreInfo?.bankRoutingNumber;
        bank.moreInfo.bankRoutingNumber = maskData({
          text: bankRoutingNumber,
          lastNum: 4
        });

        return bank;
      });

      res.status(200).json({ bankAccounts: maskedAccounts });
    } catch (error) {
      next(error);
    }
  },

  validateBankAccountInfo: (req, res, next) => {
    const bankAccountType = get(req.body, [
      'bankAccountType'
    ]) as BANK_ACCOUNTS_TYPE;

    switch (bankAccountType) {
      case BANK_ACCOUNTS_TYPE.CHECKING:
      case BANK_ACCOUNTS_TYPE.SAVING:
        const { bankRoutingNumber, bankAccountNumber } = req.body;
        if (!bankRoutingNumber || !bankAccountNumber)
          return next(
            new Error('BankRoutingNumber / BankAccountNumber is required field')
          );
        // DEMO CASE FAIL VERIFY
        const errFields = [];
        let message = '';
        if (parseFloat(bankRoutingNumber) === 0) {
          message = 'Bank Routing Number is invalid.';
          errFields.push('bankRoutingNumber');
        }
        if (parseFloat(bankAccountNumber) === 0) {
          message = ' Bank Account Number is invalid.';
          errFields.push('bankAccountNumber');
        }
        if (errFields.length === 2) {
          message = 'Bank Routing Number and Bank Account Number are invalid.';
        }

        if (!isEmpty(errFields)) {
          return gen453Code(res, message, errFields);
        }
        return res.status(200).json({
          bankAccountType,
          bankRoutingNumber,
          bankAccountNumber,
          bankName: 'Texas Capital Bank',
          bankCity: 'Richardson',
          bankState: 'TX - Texas'
        });
      case BANK_ACCOUNTS_TYPE.DEBIT:
        const { cardDebitNumber, expirationDateDebitCard } = req.body;
        if (!cardDebitNumber || !expirationDateDebitCard)
          return next(
            new Error(
              'cardDebitNumber / expirationDateDebitCard is required field'
            )
          );
        if (parseFloat(cardDebitNumber) === 0)
          return gen453Code(res, 'Card Number is invalid.', [
            'cardDebitNumber'
          ]);

        return res.status(200).json({
          bankAccountType,
          cardDebitNumber,
          expirationDateDebitCard
        });
      case BANK_ACCOUNTS_TYPE.CREDIT:
        const { cardCreditNumber, expirationDateCreditCard } = req.body;
        if (!cardCreditNumber || !expirationDateCreditCard)
          return next(
            new Error(
              'cardCreditNumber / expirationDateCreditCard is required field'
            )
          );
        if (parseFloat(cardCreditNumber) === 0)
          return gen453Code(res, 'Card Number is invalid.', [
            'cardCreditNumber'
          ]);

        return res.status(200).json({
          bankAccountType,
          cardCreditNumber,
          expirationDateCreditCard
        });

      default:
        return next(new Error('bankAccountType is NOT CORRECT'));
    }
  },
  registerBankAccount: (req, res, next) => {
    const { accountId } = req.body;
    const bankAccountType = get(req.body, [
      'bankAccountType'
    ]) as BANK_ACCOUNTS_TYPE;

    let newBankAccount;
    let errors;

    switch (bankAccountType) {
      case BANK_ACCOUNTS_TYPE.CHECKING:
      case BANK_ACCOUNTS_TYPE.SAVING:
        const {
          bankRoutingNumber,
          bankAccountNumber,
          bankName,
          bankCity,
          bankState
        } = req.body;
        if (!bankRoutingNumber || !bankAccountNumber) {
          errors = new Error(
            'BankRoutingNumber / BankAccountNumber is required field'
          );
          break;
        }
        newBankAccount = {
          accountNumber: bankAccountNumber,
          accountType: BANK_ACCOUNTS[bankAccountType],
          moreInfo: {
            bankRoutingNumber,
            bankName,
            bankCity,
            bankState
          }
        };
        break;
      case BANK_ACCOUNTS_TYPE.CREDIT:
        const { cardCreditNumber, expirationDateCreditCard } = req.body;
        if (!cardCreditNumber || !expirationDateCreditCard) {
          errors = new Error(
            'cardCreditNumber / expirationDateCreditCard is required field'
          );
          break;
        }
        if (new Date(expirationDateCreditCard) < new Date())
          return gen453Code(res, 'Debit account failed to add.');

        newBankAccount = {
          accountNumber: cardCreditNumber,
          accountType: BANK_ACCOUNTS[bankAccountType],
          moreInfo: {
            bankRoutingNumber: '',
            bankName: '',
            bankCity: '',
            bankState: ''
          },
          expirationDateCreditCard
        };
        break;
      case BANK_ACCOUNTS_TYPE.DEBIT:
        const { cardDebitNumber, expirationDateDebitCard } = req.body;
        if (!cardDebitNumber || !expirationDateDebitCard) {
          errors = new Error(
            'cardDebitNumber / expirationDateDebitCard is required field'
          );
          break;
        }
        if (new Date(expirationDateDebitCard) < new Date())
          return gen453Code(res, 'Debit account failed to add.');

        newBankAccount = {
          accountNumber: cardDebitNumber,
          accountType: BANK_ACCOUNTS[bankAccountType],
          moreInfo: {
            bankRoutingNumber: '',
            bankName: '',
            bankCity: '',
            bankState: ''
          },
          expirationDateDebitCard
        };
        break;
      default:
        errors = new Error('bankAccountType is NOT CORRECT');
        break;
    }
    if (newBankAccount && !errors) {
      const updatedBankAccounts = getBankAccountByAccountId(accountId).concat(
        newBankAccount as any
      );
      updateBankAccountDataByAccountId(accountId, updatedBankAccounts);
      return res.status(200).json({ message: 'added successfully' });
    }
    next(errors);
  },

  getDetailPaymentInfo: (req, res) => {
    const paymentDetail = {
      amountInfo: [
        { value: 25.0, label: 'Minimum Payment Due' },
        { value: 0, label: 'Last Statement Balance' },
        { value: 0, label: 'Current Balance' },
        { value: 0, label: 'Past Due Amount' }
      ],
      paymentDueDate: faker.date.future()
    };
    res.status(200).json({ ...paymentDetail });
  },

  getDetailAccountPaymentInfo: (req, res) => {
    const { accountId, eValue } = req.body;
    const bankAccounts = getBankAccountByAccountId(accountId);
    res
      .status(200)
      .json(
        bankAccounts.find(item => hashValue(item.accountNumber) === eValue)
      );
  }
};

// --------------PAYMENTS-------------------------

const getPaymentDataByAccountId = async (accountId: string) => {
  const firstCharId = (accountId as string).charAt(0);
  const existedPayments = get(paymentData, [firstCharId]);
  if (existedPayments) return existedPayments;

  try {
    const data = (await mockGeneratorPromise(
      '/payment/payments.json'
    )) as PaymentResponse;
    const { payments } = data;
    // modify generated data with business rules
    const formattedData = formatDataFollowBusiness(payments);
    set(paymentData, [firstCharId], formattedData);
    return formattedData;
  } catch (error) {
    throw error;
  }
};

const updatePaymentDataByAccountId = (
  accountId: string,
  updatedPayments: PaymentItem[]
) => {
  const firstCharId = (accountId as string).charAt(0);
  set(paymentData, [firstCharId], updatedPayments);
};

// -----------------------BANK ACCOUNT HELPER------------------------------------------

const getBankAccountByAccountId = (accountId: string) => {
  const firstCharId = (accountId as string).charAt(0);
  const existBankAccount = get(bankAccountData, [firstCharId]);
  if (existBankAccount) return existBankAccount;

  let bankAccounts: typeof mockBankAccounts = mockBankAccounts;

  if (accountId === MOCK_ACCOUNT_ID.PAYMENT_NO_DATA_FOUND) {
    bankAccounts = [];
  }
  // no Debit Card
  if (accountId === MOCK_ACCOUNT_ID.PAYMENT_NO_DEBIT_CARD) {
    bankAccounts = mockBankAccounts.filter(
      item => item.accountType.code !== 'D'
    );
  }

  if (accountId === MOCK_ACCOUNT_ID.PAYMENT_NO_CREDIT_CARD) {
    bankAccounts = mockBankAccounts.filter(
      item => item.accountType.code !== 'CR'
    );
  }

  set(bankAccountData, [firstCharId], bankAccounts);

  return bankAccounts;
};

const updateBankAccountDataByAccountId = (
  accountId: string,
  updatedBankAccount: any
) => {
  const firstCharId = (accountId as string).charAt(0);
  set(bankAccountData, [firstCharId], updatedBankAccount);
};

// handle business rules
/**
 * initialDate truoc scheduleDate 1 tuan
 * Bank Routing Number,Bank Name: this field is blank for pending debit card payments (Mdash)
 */

const formatDataFollowBusiness = (data: PaymentItem[]) => {
  // initialDate before scheduleDate 1-7 days
  // add bankAccount Infor
  if (!isArray(data)) return [];

  // do not include credit account into list payment
  const bankAccountsWithoutCredit = maskBankAccountNumbers(
    mockBankAccounts.filter(item => item.accountType.code !== 'CR'),
    9
  );

  return orderBy<PaymentItem>(
    data,
    [item => new Date(item.schedulePaymentDate)],
    ['desc']
  ).map(item => {
    const { schedulePaymentDate } = item;

    // initialDate before scheduleDate 1-7 days
    const _scheduledDate = new Date(schedulePaymentDate);
    const initiatedDate = new Date(
      _scheduledDate.getTime() - faker.random.number(7) * 24 * 60 * 60 * 1000
    );
    set(item, ['moreInfo', 'initiatedDate'], initiatedDate);

    // random Accounts from the list
    const randomBankAccount = bankAccountsWithoutCredit[faker.random.number(2)];

    const { accountNumber, accountType, moreInfo } = randomBankAccount;
    set(item, ['paymentAccountNumber'], accountNumber);
    set(item, ['paymentType'], accountType);
    const existMoreInfo = get(item, ['moreInfo'], {});
    set(item, ['moreInfo'], { ...existMoreInfo, ...moreInfo });
    return item;
  });
};

// mask account Number before send back to client
const maskBankAccountNumbers = (
  bankAccounts: typeof mockBankAccounts,
  digits?: number
) => {
  return cloneDeep(bankAccounts).map(bank => {
    const accountType = bank?.accountType?.code as BANK_ACCOUNTS_TYPE;
    const accountNumber = bank?.accountNumber;
    switch (accountType) {
      // if checking and saving mask last 4
      case BANK_ACCOUNTS_TYPE.CHECKING:
      case BANK_ACCOUNTS_TYPE.SAVING:
        set(
          bank,
          ['accountNumber', 'maskedValue'],
          maskData({
            text: getLastNumOfChar(accountNumber, digits),
            lastNum: 4
          })
        );

        break;
      // if credit and debit - mask first 6 - last 4
      case BANK_ACCOUNTS_TYPE.CREDIT:
      case BANK_ACCOUNTS_TYPE.DEBIT:
        set(
          bank,
          ['accountNumber', 'maskedValue'],
          maskData({
            text: getLastNumOfChar(accountNumber, digits),
            lastNum: 4,
            firstNum: digits ? 0 : 6
          })
        );
        break;
      default:
        break;
    }
    set(bank, ['accountNumber', 'eValue'], hashValue(accountNumber));
    return bank;
  });
};
