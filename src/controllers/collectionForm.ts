// data reference
import callResultRefData from './referenceData/callResultRefData.json';

// helpers
import { Handler } from 'express';
import { gen400, addDays, addMonths, mockGenerator } from '../helpers';
import { get, set, isEmpty, pick } from 'lodash';

// types
import { CALL_RESULTS_TYPE, INTERVAL_TYPE } from '../types';

const { bankruptcyRef, promiseToPayRef } = callResultRefData;
const collectionFormData = {};

const findBankruptcyRef = (accountId: string) => {
  const existedBankruptcyData = get(collectionFormData, [
    CALL_RESULTS_TYPE.BANKRUPTCY,
    accountId
  ]);
  const isConfirmedStep = get(collectionFormData, [
    CALL_RESULTS_TYPE.BANKRUPTCY,
    accountId,
    'confirmed'
  ]);

  if (isEmpty(existedBankruptcyData) && !isConfirmedStep)
    return bankruptcyRef.find((item: any) => item.step === 0);

  if (!isConfirmedStep)
    return bankruptcyRef.find((item: any) => item.step === 1);

  return bankruptcyRef.find((item: any) => item.step === 2);
};

export const collectionFormController: Record<string, Handler> = {
  getContactReferData: (req, res) => {
    const { accountId } = req.body;
    if (!accountId) {
      return gen400(res, { message: 'AccountId is required' });
    }

    mockGenerator(
      'collectionForm/collectionForContactRefData.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        res.send(data);
      }
    );
  },
  getCallResultReferData: (req, res) => {
    const { accountId } = req.body;
    const refData = [];
    if (!accountId) {
      return gen400(res, { message: 'AccountId is required' });
    }
    refData.push(findBankruptcyRef(accountId));
    refData.push(promiseToPayRef[0]);
    res.send({ data: refData });
  },
  checkCallResultStatus: (req, res) => {
    const { accountId, callResultType } = req.body;
    if (!accountId || !callResultType) {
      return gen400(res, {
        message: 'AccountId and callResultType are required'
      });
    }
    const existedData = get(collectionFormData, [callResultType, accountId]);
    if (existedData) {
      return res.send({ data: existedData });
    }
    res.send({ data: null });
  },
  updateCallResultStatus: (req, res) => {
    const { accountId, callResultType, data } = req.body;
    const intervalId = get(data, 'interval.id') as INTERVAL_TYPE;

    // handle interval recursing for Promise To Pay
    if (callResultType === CALL_RESULTS_TYPE.PROMISE_TO_PAY && intervalId) {
      const countValue = parseInt(get(data, 'count.value')) - 1;
      const paymentDate = get(data, 'paymentDate');
      let endDate;
      switch (intervalId) {
        case INTERVAL_TYPE.WEEKLY:
          endDate = addDays(new Date(paymentDate), countValue * 7);
          break;
        case INTERVAL_TYPE.BIWEEKLY:
          endDate = addDays(new Date(paymentDate), countValue * 14);
          break;
        case INTERVAL_TYPE.MONTHLY:
          endDate = addMonths(new Date(paymentDate), countValue);
          break;
        default:
          break;
      }
      data.endDate = endDate;
      data.startDate = paymentDate;
    }

    set(collectionFormData, [callResultType, accountId], data);
    res.send({
      data: data
    });
  }
};
