import { Handler } from 'express';
import { pick } from 'lodash';
import { gen400, mockGenerator } from '../helpers';

export const transactionController: Record<string, Handler> = {
  getPendingAuthTransactionDetail: (req, res) => {
    const { transactionCode, accountId } = req.body;

    if (!transactionCode || !accountId) {
      return gen400(res, {
        message: 'accountId & transactionCode is required'
      });
    }

    mockGenerator(
      'transaction/pendingAuthorizationDetail.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        res.send(data);
      }
    );
  },
  getPendingAuthTransactions: (req, res) => {
    const { startSequence, endSequence, accountId } = req.body;
    if (!startSequence) {
      return res.status(404).send({ message: 'startSequence required' });
    }
    mockGenerator('transaction/pendingAuthTransactions.json', (error, data) => {
      if (error) {
        const { message, stack } = pick(error, 'message', 'stack');
        return res.status(500).send({ message, stack });
      }
      if (accountId === '1200000000000000') {
        return res.send([]);
      }
      if (parseInt(startSequence) > 25) {
        return res.send(data.customers.slice(50, 60));
      }
      return res.send(
        data.customers.slice(parseInt(startSequence), parseInt(endSequence))
      );
    });
  },
  statementMonthList: (req, res) => {
    mockGenerator('transaction/statementMonthRefData.json', (error, data) => {
      if (error) {
        const { message, stack } = pick(error, 'message', 'stack');
        return res.status(500).send({ message, stack });
      }
      return res.send(data.statementsList);
    });
  },
  getStatementOverview: (req, res) => {
    const { accountId, statementDate } = req.body;
    if (!accountId && !statementDate) {
      return res.send('Not found accounts...');
    }

    if (accountId === 0) {
      return res.send({ customers: [] });
    }

    mockGenerator('transaction/statementSummary.json', (error, data) => {
      if (error) {
        const { message, stack } = pick(error, 'message', 'stack');
        return res.status(500).send({ message, stack });
      }

      return res.send(data);
    });
  },
  getTransactionDetail: (req, res) => {
    mockGenerator(
      'transaction/accountDetailStatementDetail.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        return res.send(data);
      }
    );
  },
  getTransactionList: (req, res) => {
    const { statementDate, startSequence, accountId } = req.body;
    if (!statementDate || !startSequence) {
      return res
        .status(404)
        .send({ message: 'statementDate and startSequence  required' });
    }
    mockGenerator('transaction/statementTransactions.json', (error, data) => {
      if (error) {
        const { message, stack } = pick(error, 'message', 'stack');
        return res.status(500).send({ message, stack });
      }
      if (accountId === '1200000000000000') {
        return res.send([]);
      }
      if (startSequence === '0') {
        return res.send(
          data.data[statementDate].slice(0, 25).map((item: any) => {
            const { transactionDate } = item;
            return {
              ...item,
              transactionDate: new Date(transactionDate),
              postDate: new Date(transactionDate)
            };
          })
        );
      }
      if (startSequence === '25') {
        return res.send(
          data.data[statementDate].slice(25, 50).map((item: any) => {
            const { transactionDate } = item;
            return {
              ...item,
              transactionDate: new Date(transactionDate),
              postDate: new Date(transactionDate)
            };
          })
        );
      }
      if (startSequence !== '0' && startSequence !== '25') {
        return res.send([]);
      }
    });
  },
  getAccountStatementHistory: (req, res) => {
    mockGenerator(
      'transaction/historicalInformationStatement.json',
      (error, data) => {
        if (error) {
          const { message, stack } = pick(error, 'message', 'stack');
          return res.status(500).send({ message, stack });
        }
        return res.send(data.customers);
      }
    );
  }
};
