import { Handler } from 'express';
import { mockGenerator, gen400 } from '../helpers';
import { pick } from 'lodash';

import refDataOptData from '../controllers/referenceData/referenceDataOptions.json';

export const referenceDataController: Record<string, Handler> = {
  getStateListRefData: (req, res) => {
    mockGenerator('stateRefData.json', (error, data) => {
      if (error) {
        const { message, stack } = pick(error, 'message', 'stack');
        return res.status(500).send({ message, stack });
      }
      res.send(data);
    });
  },
  getReferenceDataOptions: (req, res) => {
    const { componentID } = req.body;
    if (!componentID) {
      return gen400(res, { message: 'componentID is required' });
    }
    const data = (refDataOptData as any)[componentID] ?? [];
    res.send(data);
  }
};
