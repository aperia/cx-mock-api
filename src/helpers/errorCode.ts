import { Response } from 'express';
import { every } from 'lodash';

// bad request
export const gen400 = (res: Response<any>, warning: Record<string, string>) => {
  return res.status(400).send(JSON.stringify(warning));
};

export const gen453Code = (
  res: Response<any>,
  message: string,
  errorFields?: Array<string>
) => {
  return res.status(400).send(
    JSON.stringify({
      coreResponse: {
        responseCode: 453,
        message,
        errorFields
      }
    })
  );
};

export const gen455Code = (res: Response<any>, message: string) => {
  return res.status(400).send(
    JSON.stringify({
      coreResponse: {
        responseCode: 455,
        message
      }
    })
  );
};

export const generateMessage = (data: Record<string, string>) => {
  return Object.keys(data)
    .filter(key => !data[key])
    .join(', ');
};

export const checkRequireFields = (data: Record<string, string>) => {
  return every(Object.values(data));
};
