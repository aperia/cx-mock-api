export const getLastNumOfChar = (
  value: string | number,
  digits?: number
): string => {
  if (!digits) return value.toString();
  const valueStr = value.toString();
  const length = valueStr.length;
  return valueStr.substring(length - digits);
};
