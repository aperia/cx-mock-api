import { Router, Express } from 'express';
import { VIRTUAL_DIR_PATH, API_VERSION } from '../configs';

const baseApiUrl = VIRTUAL_DIR_PATH + API_VERSION;

export const buildRouter = (app: Express, route: Record<string, Router>) => {
  const initRoute: Array<Router> = [];
  for (const key in route) {
    const [routeName] = key.toLowerCase().split('route');
    if (routeName) {
      console.log('route');
      app.use(`${baseApiUrl}/${routeName}`, route[key]);
    }
  }
  return initRoute;
};
