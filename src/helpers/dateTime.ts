export const addDays = (baseDate: Date, days: number) =>
  new Date(baseDate.getTime() + days * 24 * 60 * 60 * 1000);

export const addMonths = (baseDate: Date, months: number) =>
  new Date(baseDate.setMonth(baseDate.getMonth() + +months));

export const getTimeValueOfStartDate = (
  date: string,
  addMoreDate?: number
): number => {
  if (!date) return 0;
  const dateTime = new Date(date);
  const startOfDay = new Date(
    dateTime.getFullYear(),
    dateTime.getMonth(),
    dateTime.getDate()
  );
  if (addMoreDate) return addDays(startOfDay, 1).valueOf();
  return startOfDay.valueOf();
};

export const isDateBetween = (
  compareDate: string,
  startDate: string,
  endDate: string
): boolean => {
  if (!compareDate) return false;
  if (!startDate) return true;
  const _compareDate = new Date(compareDate).valueOf();
  const _startDate = getTimeValueOfStartDate(startDate);
  const _endDate = endDate
    ? getTimeValueOfStartDate(endDate, 1)
    : getTimeValueOfStartDate(startDate, 1);
  return _compareDate >= _startDate && _compareDate <= _endDate;
};
