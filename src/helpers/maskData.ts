import { isEmpty } from 'lodash';
/**
 * firstNum  = 6 -> 255424••••••••••••••
 * lastNum = 4 -> 255424•••••••1252
 */

export const maskData = (data: {
  text: string;
  firstNum?: number;
  lastNum?: number;
}): string => {
  const { firstNum, lastNum, text } = data;
  if (isEmpty(text)) return '';
  const maskData = text.split('') as any[];
  const start = firstNum ? firstNum : 0;
  const end = lastNum ? maskData.length - lastNum : maskData.length;
  maskData.splice(start, end - start, ...Array(end - start).fill('•'));
  return maskData.join('');
};
