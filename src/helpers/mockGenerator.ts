import faker from 'faker';
import fs from 'fs';
import path from 'path';
import {
  get,
  isFunction,
  isNil,
  isBoolean,
  isNumber,
  set,
  isObject
} from 'lodash';

// add more function to faker module
const mock = faker as any;
mock.custom = {};
mock.custom.getOne = (...args: any[]) => {
  return faker.helpers.randomize(args);
};
mock.custom.getUTCDate = (date: string) => {
  return new Date(date).toISOString();
};
mock.custom.get = (path: string, obj: any) => {
  return get(obj, path);
};
mock.custom.emptyAble = (value: any, chance = 0.5) => {
  const randomNumber = Math.random();
  return randomNumber > chance ? value : null;
};
mock.custom.randomFloat = (maxNumber: any, fractionDigit: any = 2) => {
  return (Math.random() * parseInt(maxNumber)).toFixed(parseInt(fractionDigit));
};
// handle config and get new data
const handleConfig = (data: any) => {
  const config = get(data, '_config');
  if (!config) return data;

  const nextData = JSON.parse(JSON.stringify(data));

  Object.entries(config).map(([path, value]) => {
    let count = get(value, 'count');

    // handle loop
    if (isNumber(count)) {
      const dataHasStatementTemplate = get(nextData, `${path}.0`);
      const listDataHasStatementTemplate = get(nextData, path);
      if (!Array.isArray(listDataHasStatementTemplate)) return;
      if (count === 0) {
        return set(nextData, path, []);
      }

      while (count-- > 1) {
        listDataHasStatementTemplate.push(dataHasStatementTemplate);
      }
    }
  });

  return nextData;
};

const parseDataType = (
  statement: string,
  statementTemplate: string,
  jsonStr: string,
  // using for eval, very very important, please don't remove it
  nextData?: any
) => {
  const evalStr = `${statement}`;
  let value = eval(evalStr);

  if (isFunction(value)) value = value();

  const isNum = value !== null && typeof +value === 'number' && !isNaN(+value);
  const isBool = isBoolean(value);
  const isNullOrUndefined = isNil(value);
  const isObj = isObject(value);

  let nextJsonStr = jsonStr.replace(statementTemplate, value);

  // avoid if number is 0123456 => JSON.parse bug
  // convert 0123456 -> 123456
  if (isNum) value = +value;

  if (isNum || isBool || isNullOrUndefined) {
    nextJsonStr = nextJsonStr.replace(`"${value}"`, value);
  }

  if (isObj) {
    nextJsonStr = nextJsonStr.replace(`"${value}"`, JSON.stringify(value));
  }

  try {
    const nextData = JSON.parse(nextJsonStr);
    return { nextData, nextJsonStr };
  } catch (error) {
    console.group();
    console.log('error...', error);
    console.log('statementTemplate...', statementTemplate);
    console.log('statement...', statement);
    console.log('value...', value);
    console.groupEnd();
    throw error;
  }
};

const parsing = (data: any) => {
  let nextData = JSON.parse(JSON.stringify(data));

  // handle config
  nextData = handleConfig(nextData);
  let jsonStr = JSON.stringify(nextData);

  const statementTemplateList = jsonStr.match(/(<%).*?(%>)/gm);
  if (!statementTemplateList) return nextData;

  // support handle mock.custom.get action
  const getActions = [];

  // handle normal parsing
  for (let index = 0; index < statementTemplateList.length; index++) {
    const statementTemplate = statementTemplateList[index];
    const statement = statementTemplate.replace(/<%|%>/gm, '');

    if (statementTemplate.indexOf('mock.custom.get(') !== -1) {
      getActions.push({
        statement: `${statement}`.replace(')', ',nextData)'),
        statementTemplate
      });
      continue;
    }

    const { nextData: nextDataParsed, nextJsonStr } = parseDataType(
      statement,
      statementTemplate,
      jsonStr
    );

    jsonStr = nextJsonStr;
    nextData = nextDataParsed;
  }

  // handle mock.custom.get action
  for (let index = 0; index < getActions.length; index++) {
    const { nextData: nextDataParsed, nextJsonStr } = parseDataType(
      getActions[index].statement,
      getActions[index].statementTemplate,
      jsonStr,
      nextData
    );

    jsonStr = nextJsonStr;
    nextData = nextDataParsed;
  }

  return nextData;
};

export const mockGenerator = (
  fileName: string,
  callback?: (error?: any, data?: any) => void
) => {
  fs.readFile(
    path.join(__dirname, '..', 'controllers', 'mock-json', fileName),
    (error, data) => {
      try {
        const dataObjFromBuffer = JSON.parse(data as any);
        const result = parsing(dataObjFromBuffer);
        delete (result as Record<string, any>)['_config'];

        isFunction(callback) && callback(undefined, result);
      } catch (error) {
        isFunction(callback) && callback(error);
      }
    }
  );
};
