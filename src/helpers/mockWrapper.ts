import { mockGenerator } from './mockGenerator';

export const mockGeneratorPromise = <T>(path: string) => {
  return new Promise<T>((resolve, reject) => {
    mockGenerator(path, (error, data) => {
      if (error) {
        return reject(error);
      }
      return resolve(data);
    });
  });
};
