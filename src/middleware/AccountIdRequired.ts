import { RequestHandler } from 'express';

export const AccountIdRequired: RequestHandler = (req, res, next) => {
  const { accountId } = req.body;

  if (!accountId) {
    return next(new Error('AccountId is required'));
  }
  next();
};
