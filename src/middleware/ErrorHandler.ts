import { ErrorRequestHandler } from 'express';

export const ErrorHandler: ErrorRequestHandler = (err, req, res, next) => {
  console.log(err);

  if (err) {
    const { message = '' } = err;
    return res.status(400).json({ type: 'error', message });
  }
  next();
};
