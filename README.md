## How to use mock-api

### cd to mock-api folder

### 1. Unzip node_modules.7z

### 2. npm start

### 3. Try apis by using PostMan

- Login api, jwt will be stored in cookie, you can modify timelife of the cookie or jwt in the .env file
- you can find user to login in users.json file.
- password is: userName + " " + "pwd", example: "pphan pwd"
  http://localhost:10000/api/cxc/login

- Search accounts, search by accountNumber
  request body example: { "accountNumber": "pphan" }
  http://localhost:10000/api/cxc/searchAccount

* Get account details, get by accountNumber
  request body example: { "accountNumber": "pphan" }
  http://localhost:10000/api/cxc/getAccountDetails
